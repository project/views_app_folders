<?php

/**
 * @file
 * Contains the App Folders Grid style plugin.
 */

/**
 * Style plugin to render each item in an unordered list.
 *
 * @ingroup views_style_plugins
 */
class ViewsAppFoldersPluginStyleAppFolders extends views_plugin_style {
  /**
   * Set default options.
   */
  public function option_definition() {
    $options = parent::option_definition();

    $options['type'] = array('default' => 'ul');
    $options['class'] = array('default' => '');
    $options['wrapper_class'] = array('default' => 'item-list');
    $options['thumbnail_field'] = array('default' => '');
    $options['grid_cols'] = array('default' => 5);
    $options['opacity'] = array('default' => 0.2);
    $options['close_button'] = array('default' => TRUE);
    $options['adjust_margin_top'] = array('default' => TRUE);
    $options['margin_top_base'] = array('default' => 0);
    $options['margin_top_increment'] = array('default' => 50);
    $options['animation_speed'] = array('default' => 200);
    $options['insta_switch'] = array('default' => FALSE);
    return $options;
  }

  /**
   * Style plugin options form.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['grid_cols'] = array(
      '#title' => t('Grid columns'),
      '#description' => t('Number of columns of the grid.'),
      '#type' => 'textfield',
      '#size' => '5',
      '#default_value' => $this->options['grid_cols'],
    );

    $form['wrapper_class'] = array(
      '#title' => t('Wrapper class'),
      '#description' => t('The class to provide on the wrapper, outside the list.'),
      '#type' => 'textfield',
      '#size' => '30',
      '#default_value' => $this->options['wrapper_class'],
    );
    $field_list = array();
    $field_list = $this->display->handler->get_field_labels(TRUE);

    $default_field = $this->options['thumbnail_field'];
    if (empty($default_field)) {
      if (isset($this->display->handler)) {
        foreach ($this->display->handler->get_handlers('field') as $field => $handler) {
          if (empty($default_field)) {
            $default_field = $field;
          }
          if (isset($handler->field_info['type']) && ($handler->field_info['type'] == 'image')) {
            $default_field = $field;
          }
        }
      }
    }

    $form['thumbnail_field'] = array(
      '#title' => t('Thumbnail Field'),
      '#description' => t('The field to be shown as thumbnail in the grid.'),
      '#type' => 'select',
      '#options' => $field_list,
      '#default_value' => $default_field,
    );

    $form['advanced_settings'] = array(
      '#title' => t('Advanced settings'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['animation_speed'] = array(
      '#fieldset' => 'advanced_settings',
      '#title' => t('Animation speed'),
      '#description' => t('Time (in ms) for transitions.'),
      '#type' => 'textfield',
      '#size' => '8',
      '#default_value' => $this->options['animation_speed'],
    );

    $form['opacity'] = array(
      '#fieldset' => 'advanced_settings',
      '#title' => t('Opacity'),
      '#description' => t('Opacity of non-selected items.'),
      '#type' => 'textfield',
      '#size' => '8',
      '#default_value' => $this->options['opacity'],
    );

    $form['insta_switch'] = array(
      '#fieldset' => 'advanced_settings',
      '#title' => t('One-click folder switching'),
      '#description' => t('Enable one-click folder switching rather than iOS-like two clicks.'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['insta_switch'],
    );

    $form['close_button'] = array(
      '#fieldset' => 'advanced_settings',
      '#title' => t('Close button'),
      '#description' => t('Add close button on folder.'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['close_button'],
    );

    $form['margin_top_adjust'] = array(
      '#fieldset' => 'advanced_settings',
      '#title' => t('Adjust the margin-top'),
      '#description' => t('Adjust the margin-top for the folder area based on row selected.'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['margin_top_adjust'],
    );

    $form['margin_top_base'] = array(
      '#fieldset' => 'advanced_settings',
      '#title' => t('Margin top base'),
      '#description' => t('The natural margin-top for the area.'),
      '#type' => 'textfield',
      '#size' => '8',
      '#default_value' => $this->options['margin_top_base'],
      '#states' => array(
        'visible' => array(
          ':input[name="style_options[margin_top_adjust]"]' => array('checked' => TRUE),
        ),
      ),
    );

    $form['margin_top_increment'] = array(
      '#fieldset' => 'advanced_settings',
      '#title' => t('Margin top increment'),
      '#description' => t('The absolute value of the increment of margin-top per row.'),
      '#type' => 'textfield',
      '#size' => '8',
      '#default_value' => $this->options['margin_top_increment'],
      '#states' => array(
        'visible' => array(
          ':input[name="style_options[margin_top_adjust]"]' => array('checked' => TRUE),
        ),
      ),
    );

  }

  /**
   * Render the style output.
   */
  public function render() {
    $library = libraries_load('jquery_app_folders');
    if (empty($library['loaded'])) {
      drupal_set_message($library['error message'], 'error', FALSE);
      drupal_set_message(t('Please install the jquery_app_folders library so that the .js file is accessible at sites/*/libraries/jquery_app_folders/jquery.app-folders.js.'), 'error', FALSE);
      return;
    }
    if (!$this->uses_fields()) {
      drupal_set_message(t('Please choose "Show: Fields" in the views format settings.'), 'error', FALSE);
      return;
    }
    return parent::render();
  }
}
