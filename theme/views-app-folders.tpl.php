<?php

/**
 * @file
 * Default simple view template to display app folders.
 *
 * @ingroup views_templates
 */
?>
<?php print (!empty($wrapper_prefix)) ? $wrapper_prefix : ''; ?>
<?php if (isset($thumbnails)): ?>
<?php foreach ($thumbnails as $grid_rowi_id => $grid_row): ?>
<div class='jaf-row jaf-container'>
<?php foreach ($grid_row as $id => $thumbnail): ?>
 <div id="vaf-<?php print (!empty($dom_id)) ? $dom_id : '0'; ?>-<?php print $id ?>" class="folder <?php print (!empty($row_classes)) ? $row_classes[$id] : ''; ?>">
  <a href="javascript:;" name="vaf-row-<?php print $id ?>"><?php print $thumbnail; ?></a>
 </div>
<?php endforeach; ?>
</div>
<?php endforeach; ?>
<?php endif; ?>
<?php if (isset($rows)): ?>
<?php foreach ($rows as $id => $row): ?>
<div class="folderContent vaf-<?php print (!empty($dom_id)) ? $dom_id : '0'; ?>-<?php print $id ?> <?php print (!empty($class)) ? $class : '' ?>">
 <div class="jaf-container <?php print (!empty($row_classes)) ? $row_classes[$id] : ''; ?>">
<?php print $row; ?>
 </div>
 <?php if (!empty($close_button)): ?>
    <a class="jaf-close" href="javascript:;">X</a>
  <?php endif; ?>
</div>
<?php endforeach; ?>
<?php endif; ?>
<?php print (!empty($wrapper_suffix)) ? $wrapper_suffix : ''; ?>
