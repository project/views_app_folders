<?php

/**
 * @file
 * Provides Thumbnail View Expanding Preview style options for Views.
 */

/**
 * Display the view as an HTML list element.
 */
function template_preprocess_views_app_folders(&$vars) {
  // Load required libraries and styles.
  if (($library = libraries_load('jquery_app_folders')) && !empty($library['loaded'])) {
    $view = $vars['view'];
    $handler  = $vars['view']->style_plugin;
    $grid_cols = (int) check_plain($handler->options['grid_cols']);
    $thumbnail_field = $handler->options['thumbnail_field'];
    $close_button = $handler->options['close_button'];

    $class = explode(' ', check_plain($handler->options['class']));
    $class = array_map('views_clean_css_identifier', $class);
    $class[] = 'views-app-folders';

    $wrapper_class = explode(' ', check_plain($handler->options['wrapper_class']));
    $wrapper_class[] = 'app-folders-container';
    $wrapper_class = array_map('views_clean_css_identifier', $wrapper_class);

    $vars['class'] = trim(implode(' ', $class));
    $vars['wrapper_class'] = implode(' ', $wrapper_class);
    $vars['wrapper_prefix'] = '';
    $vars['wrapper_suffix'] = '';
    if ($vars['wrapper_class']) {
      $vars['wrapper_prefix'] = '<div class="' . $vars['wrapper_class'] . '">';
      $vars['wrapper_suffix'] = '</div>';
    }
    $thumbnails = array();
    $grid_col = 1;
    $grid_row = 1;
    $renderer = $view->style_plugin;
    foreach ($view->result as $id => $row) {
      $style_output = $renderer->render_fields($view->result);
      $thumbnails[$grid_row][$id] = $style_output[$id][$thumbnail_field];
      if ($grid_col >= $grid_cols) {
        $grid_row++;
        $grid_col = 1;
      }
      else {
        $grid_col++;
      }
    }
    $vars['thumbnails'] = $thumbnails;
    $vars['dom_id'] = $view->dom_id;
    $vars['close_button'] = $close_button;

    template_preprocess_views_view_unformatted($vars);
    drupal_add_css(drupal_get_path('module', 'views_app_folders') .
      "/css/views-app-folders.css", array('preprocess' => FALSE));
    drupal_add_js(drupal_get_path('module', 'views_app_folders') .
      "/js/views-app-folders.js", array('preprocess' => FALSE));
    // Add the settings for JS.
    drupal_add_js(array(
      'viewsAppFolders' => array(
        'gridCols' => $grid_cols,
        'opacity' => (float) check_plain($handler->options['opacity']),
        'adjustMarginTop' => (boolean) check_plain($handler->options['adjust_margin_top']),
        'marginTopBase' => (string) check_plain($handler->options['margin_top_base']),
        'marginTopIncrement' => (string) check_plain($handler->options['margin_top_increment']),
        'animationSpeed' => (int) check_plain($handler->options['animation_speed']),
        'instaSwitch' => (boolean) check_plain($handler->options['insta_switch']),
        'domID' => (string) check_plain('view-dom-id-'.$vars['dom_id']),
      ),
    ), 'setting');
  }
  else {
    drupal_set_message($library['error message'], 'error', FALSE);
  }
}
