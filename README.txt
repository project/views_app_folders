Views App Folders is a views style plugin making use of jQuery App Folders
plugin [ http://app-folders.com/ ] -- a thumbnail grid with expanding contents.
It is good for responsive design and working great on mobiles.

INSTALLATION
============

1. Download & Enable Libraries API module.
http://drupal.org/project/libraries

2. Download jQuery App Folders JS libraries from Github:
https://github.com/spsaucier/App-Folders/zipball/master

3. Place jQuery App Folders JS libraries into 
sites/*/libraries/jquery_app_folders so that the .js file is accessible at
sites/*/libraries/jquery_app_folders/jquery.app-folders.js

4. Enable this module (Views App Folders).

USAGE
=====

1. Create a content type.  You may want to add a field for use as the thumbnail.
   The thumbnail field is not necessary to be an image.

2. Create your new view

Using the "Add new view" form, create your new view and choose the
content type. Change the display format to "App Folders Grid". Click "Continue 
and edit" to finish setting up the new view.

3. Configuring the view

Click "Add" in the fields section of the Views interface to add the needed 
fields from your content type.

Click the App Folders Grid "settings" in format section. Edit the general
configuration of the App Folders Grid display and then select the field for
the thumbnail.  By default, the thumbnail field is the first image field.
If there is no image field in the field list, the first field is the 
default.  You could select another content field or even "Global: Custom ..."
field for the thumbnail.

Click "Save" for your view to complete the configuration. The preview display
on the Views edit interface shows the data used by App Folders Grid.
To see the App Folders Grid display, access the block or page you just created.
