<?php

/**
 * @file
 * Defines the View Style Plugins for Views Sandwish Thumbnails module.
 */

/**
 * Implements hook_views_plugins().
 */
function views_app_folders_views_plugins() {
  return array(
    'style' => array(
      'app_folders' => array(
        'title' => t('App Folders Grid'),
        'help' => t('Display the results as grid by using jQuery App Folders plugin.'),
        'handler' => 'ViewsAppFoldersPluginStyleAppFolders',
        'uses options' => TRUE,
        'uses row plugin' => TRUE,
        'uses fields' => TRUE,
        'uses grouping' => FALSE,
        'uses row class' => TRUE,
        'type' => 'normal',
        'parent' => 'list',
        'path' => drupal_get_path('module', 'views_app_folders'),
        'theme' => 'views_app_folders',
        'theme path' => drupal_get_path('module', 'views_app_folders') . '/theme',
        'theme file' => 'views-app-folders.tpl.php',
      ),
    ),
  );
}
