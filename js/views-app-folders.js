/**
 * @file
 * A JavaScript file for the views_app_folders module.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - http://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {
  Drupal.behaviors.viewsAppFolders = {
    attach: function(context) {
      // Set folder width.
      var folderWidth = Math.floor(95 / Drupal.settings.viewsAppFolders.gridCols) + '%';
      $('.jaf-container > div.folder').css('width', folderWidth);


      // Initialize the appFolders plugin.
      $('.app-folders-container', $('.'+Drupal.settings.viewsAppFolders.domID)).appFolders({
        opacity: Drupal.settings.viewsAppFolders.opacity,
        marginTopAdjust: Drupal.settings.viewsAppFolders.adjustMarginTop,
        marginTopBase: Drupal.settings.viewsAppFolders.marginTopBase,
        marginTopIncrement: Drupal.settings.viewsAppFolders.marginTopIncrement,
        animationSpeed: Drupal.settings.viewsAppFolders.animationSpeed,
        URLrewrite: true,
        internalLinkSelector: ".jaf-internal a",
        instaSwitch: Drupal.settings.viewsAppFolders.instaSwitch
      });

      // This is a hack for jQuery App Folders Plugin 0.2
      // Fixes the close button issue with the opacity not resetting
      $('.jaf-close').click(function(){
        //Reset opacity
        $(".jaf-container").find(".folder").each(function() {
          $(this).animate({ opacity: 1.00 }, Drupal.settings.viewsAppFolders.animationSpeed);
        });
      });

    }
  }
}) (jQuery, Drupal, this, this.document);
